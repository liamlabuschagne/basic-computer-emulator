#!/usr/bin/env python
import sys


class Computer(object):

    # Memory
    RAM = []
    A = 0
    B = 0
    S = 0

    # Flags
    NEGATIVE = False
    HALTED = False

    # Program Counter
    PC = 0

    # Total cylces run
    cylces = 0

    program = []

    def __init__(self, file):
        self.filePath = file

    def halt(self):
        self.HALTED = True

    def out(self, address):
        print("The output is "+str(self.RAM[int(address)]))

    def loadA(self, address):
        self.A = self.RAM[int(address)]

    def loadB(self, address):
        self.B = self.RAM[int(address)]

    def storeA(self, address):
        self.RAM[int(address)] = self.A

    def storeB(self, address):
        self.RAM[int(address)] = self.B

    def storeS(self, address):
        self.RAM[int(address)] = self.S

    def add(self):
        self.S = self.A + self.B

    def sub(self):
        self.S = self.A - self.B
        if(self.S > 0):
            self.NEGATIVE = False
        else:
            self.NEGATIVE = True

    def jump(self, line):
        self.PC = int(line)

    def jumpIfNegative(self, line):
        if(self.NEGATIVE):
            self.PC = int(line)

    def jumpIfZero(self, line):
        if(self.S == 0):
            self.PC = int(line)

    # Instructions
    instructions = {
        "hlt": halt,
        "out": out,
        "lda": loadA,
        "ldb": loadB,
        "sta": storeA,
        "stb": storeB,
        "sts": storeS,
        "add": add,
        "sub": sub,
        "jmp": jump,
        "jin": jumpIfNegative,
        "jiz": jumpIfZero
    }

    def parseFile(self):
        file = open(self.filePath, 'r')

        hasNoErrors = True
        for line in file:
            line = line.split()

            if line[0] == "RAM":
                for v in line:
                    if not v == "RAM":
                        self.RAM.append(int(v))
            else:

                isValidInstruction = False
                if(len(line) > 1):
                    for l in self.instructions:
                        if line[0] == l:
                            self.program.append([line[0], line[1]])
                            isValidInstruction = True
                        elif line[1] == l:
                            self.program.append([line[1], line[0]])
                            isValidInstruction = True

                    if not isValidInstruction:
                        print("Invalid instruction: ", str(line))
                        hasNoErrors = False
                        break
                else:
                    self.program.append([line[0]])

        file.close()

        return hasNoErrors

    def run(self):
        if self.parseFile():
            while not self.HALTED:
                if(not self.PC > len(self.program)-1):
                    textToPrint = ""
                    if len(self.program[self.PC]) > 1:
                        textToPrint = "cmd: " + \
                            self.program[self.PC][0]+" " + \
                            self.program[self.PC][1]
                        func = self.instructions[self.program[self.PC][0]]
                        arg = self.program[self.PC][1]
                        print textToPrint + " | mem: " + str(self.RAM) + " | a: " + str(self.A) + " | b: " + str(self.B) + " | s: " + str(self.S)
                        func(self, arg)
                    else:
                        textToPrint = "cmd: "+self.program[self.PC][0] + "  "
                        func = self.instructions[self.program[self.PC][0]]
                        print textToPrint + " | mem: " + str(self.RAM) + " | a: " + str(self.A) + " | b: " + str(self.B) + " | s: " + str(self.S)
                        func(self)

                    self.PC += 1
                    self.cylces += 1

                else:
                    self.halt()

            print("This program took "+str(self.cylces)+" cycles to complete.")
            print("The computer has halted.")

        else:
            print "Failed to parse file."
            print("The computer has halted.")


if(len(sys.argv) < 2):
    print "Please enter a file path"
else:
    file = sys.argv[1]
    pc = Computer(file)
    pc.run()
